using System;
using System.Threading.Tasks;
using System.Windows.Input;
using BinanceTestApp.Common.Enums;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Common.Models;
using BinanceTestApp.Contexts.Commands;
using BinanceTestApp.Database.Entities;
using BinanceTestApp.Infrastructure.Interfaces;
using BinanceTestApp.Providers;
using Microsoft.EntityFrameworkCore;

namespace BinanceTestApp.Commands
{
    public class CreateTradeStreamCommand : RuntimeRepositoryInitializer, ICommand<CreateTradeStreamContext>
    {
        public CreateTradeStreamCommand(IRuntimeRepository runtimeRepository) : base(runtimeRepository)
        {
        }

        public async Task<CommandResult> ExecuteAsync(CreateTradeStreamContext context)
        {
            CommandResult commandResult = new CommandResult();
            DbSet<TradeStream> entity = RuntimeRepository.Query<TradeStream>();
            
            try
            {
                entity.Add(context.TradeStream);
                await RuntimeRepository.SaveAsync();
                commandResult.Status = CommandResultStatus.Ok;
            }
            catch (Exception)
            {
                commandResult.Status = CommandResultStatus.Error;
            }

            return commandResult;
        }
    }
}