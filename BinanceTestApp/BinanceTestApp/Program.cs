﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using BinanceTestApp.Common.Builders;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Infrastructure.Interfaces;
using BinanceTestApp.Infrastructure.Repositories;
using log4net.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace BinanceTestApp
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            IHost host = Startup.ConfigureHost();

            ICommandBuilder commandBuilder = host.Services.GetService(typeof(ICommandBuilder)) as CommandBuilder;
            IQueryBuilder queryBuilder = host.Services.GetService(typeof(IQueryBuilder)) as QueryBuilder;
            ILoggerFactory loggerFactory = host.Services.GetService(typeof(ILoggerFactory)) as LoggerFactory;
            
            BinanceManager binanceManager = new BinanceManager(commandBuilder, queryBuilder, loggerFactory);
            
            await host.StartAsync();
            binanceManager.Start();

            Console.ReadLine();
        }
    }
}