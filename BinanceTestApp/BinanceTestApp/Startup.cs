using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BinanceExchange.API.Models.WebSocket;
using BinanceTestApp.Common.Builders;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Database.Contexts;
using BinanceTestApp.Database.Entities;
using BinanceTestApp.Database.Factories;
using BinanceTestApp.Infrastructure.Interfaces;
using BinanceTestApp.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BinanceTestApp
{
    public class Startup
    {
        public static IHost ConfigureHost()
        {
            return new HostBuilder().UseServiceProviderFactory(new AutofacServiceProviderFactory()).ConfigureServices(
                    services =>
                    {
                        ConfigureServices(services);
                        ConfigureMappings();
                    })
                .ConfigureContainer<ContainerBuilder>(ConfigureAutofacContainer)
                .UseConsoleLifetime()
                .Build();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddAutofac();
            services.AddLogging();
        }

        private static void ConfigureMappings()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<BinanceAggregateTradeData, TradeStream>()
                .ForMember("TradeId", o => o.MapFrom(p => p.AggregateTradeId))
                .ForMember("IsBuyerMaker", o => o.MapFrom(p => p.WasBuyerMaker))
                .ForMember("Ignore", o => o.MapFrom(p => p.WasBestPriceMatch)));
        }

        private static void ConfigureAutofacContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterAssemblyTypes(typeof(Startup).Assembly).AsClosedTypesOf(typeof(ICommand<>));
            containerBuilder.RegisterAssemblyTypes(typeof(Startup).Assembly).AsClosedTypesOf(typeof(IQuery<,>));

            containerBuilder.RegisterType<CommandBuilder>().As<ICommandBuilder>();
            containerBuilder.RegisterType<QueryBuilder>().As<IQueryBuilder>();
            
            containerBuilder.RegisterType<RuntimeRepository>().As<IRuntimeRepository>();
            containerBuilder.RegisterType<BinanceDatabaseContext>().As<DbContext>()
                .WithParameter("optionsBuilder", DbContextOptionsFactory.Get<BinanceDatabaseContext>())
                .InstancePerLifetimeScope();

            
            LoggerFactory loggerFactory = new LoggerFactory();

            loggerFactory.AddDebug();
            loggerFactory.AddConsole();
            
            containerBuilder.RegisterInstance(loggerFactory).As<ILoggerFactory>();
        }
    }
}