using System.Threading.Tasks;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Contexts.Queries;
using BinanceTestApp.Database.Entities;
using BinanceTestApp.Infrastructure.Interfaces;
using BinanceTestApp.Providers;
using Microsoft.EntityFrameworkCore;

namespace BinanceTestApp.Queries
{
    public class GetTradeStreamByIdQuery : RuntimeRepositoryInitializer, IQuery<GetTradeStreamByIdContext, TradeStream>
    {
        public GetTradeStreamByIdQuery(IRuntimeRepository runtimeRepository) : base(runtimeRepository)
        {
        }

        public async Task<TradeStream> QueryAsync(GetTradeStreamByIdContext context)
        {
            return await RuntimeRepository.Query<TradeStream>().FirstOrDefaultAsync(s => s.Id == context.Id);
        }
    }
}