using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Contexts.Queries;
using BinanceTestApp.Database.Entities;
using BinanceTestApp.Infrastructure.Interfaces;
using BinanceTestApp.Providers;
using Microsoft.EntityFrameworkCore;

namespace BinanceTestApp.Queries
{
    public class GetTradeStreamsQuery : RuntimeRepositoryInitializer, IQuery<GetTradeStreamsContext, List<TradeStream>>
    {
        public GetTradeStreamsQuery(IRuntimeRepository runtimeRepository) : base(runtimeRepository)
        {
        }

        public async Task<List<TradeStream>> QueryAsync(GetTradeStreamsContext context)
        {
            return await RuntimeRepository.Query<TradeStream>().ToListAsync();;
        }
    }
}