using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BinanceExchange.API;
using BinanceExchange.API.Client;
using BinanceExchange.API.Client.Interfaces;
using BinanceExchange.API.Enums;
using BinanceExchange.API.Market;
using BinanceExchange.API.Models.Request;
using BinanceExchange.API.Models.Response;
using BinanceExchange.API.Models.Response.Error;
using BinanceExchange.API.Models.WebSocket;
using BinanceExchange.API.Utility;
using BinanceExchange.API.Websockets;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Common.Models;
using BinanceTestApp.Contexts.Commands;
using BinanceTestApp.Contexts.Queries;
using BinanceTestApp.Database.Entities;
using BinanceTestApp.Queries;
using log4net;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BinanceTestApp
{
    public class BinanceManager
    {
        private ICommandBuilder _commandBuilder;

        private IQueryBuilder _queryBuilder;
        
        private ILogger<BinanceManager> _logger;

        public BinanceManager(ICommandBuilder commandBuilder, IQueryBuilder queryBuilder, ILoggerFactory loggerFactory)
        {
            _commandBuilder = commandBuilder;
            _queryBuilder = queryBuilder;
            _logger = loggerFactory.CreateLogger<BinanceManager>();
        }

        public void Start()
        {
            _logger.Log(LogLevel.Information, "Binance API client initialization started.");
            
            var client = new BinanceClient(new ClientConfiguration()
            {
                ApiKey = "kb8EaFfwSYHQ2YIWD1m3qBnzZoNlBfkyLZhsNhpqneiwch6KbX1bDLltPY5GFMOy",
                SecretKey = "TvlUJl8i2bfXMsume4DnJZEOqJSKvtdxDDQkA4L9unT9OIB8k0mbmmdku94SdgUO"
            });

            _logger.Log(LogLevel.Information, "Subscribing to Binance websocket.");

            try
            {
                InstanceBinanceWebSocketClient binanceWebSocket = new InstanceBinanceWebSocketClient(client);
                Guid socketId = binanceWebSocket.ConnectToTradesWebSocket(TradingPairSymbols.BTCPairs.ETH_BTC,
                    async data =>
                    {
                        _logger.Log(LogLevel.Information, "Received data from TradeStream");
                        _logger.Log(LogLevel.Information, $"Start saving --- {data.Symbol} --- {data.AggregateTradeId} --- {data.TradeTime}.");

                        TradeStream tradeStream = Mapper.Map<BinanceAggregateTradeData, TradeStream>(data);

                        CommandResult commandResult = await _commandBuilder.ExecuteAsync(new CreateTradeStreamContext()
                        {
                            TradeStream = tradeStream
                        });

                        _logger.Log(LogLevel.Information, $"Data saved with status: {commandResult.Status.ToString()}");
                    });

                Thread.Sleep(10000);

                _logger.Log(LogLevel.Information, "Closing Binance websocket.");
                binanceWebSocket.CloseWebSocketInstance(socketId);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
            }
            
            _logger.Log(LogLevel.Information, "Data receiving finished.");
        }
    }
}