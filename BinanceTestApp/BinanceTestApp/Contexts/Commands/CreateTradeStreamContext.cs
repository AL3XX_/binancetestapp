using BinanceTestApp.Common.Contexts;
using BinanceTestApp.Database.Entities;

namespace BinanceTestApp.Contexts.Commands
{
    public class CreateTradeStreamContext : CommandContext
    {
        public TradeStream TradeStream { get; set; }
    }
}