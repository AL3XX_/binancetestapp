using BinanceTestApp.Common.Contexts;

namespace BinanceTestApp.Contexts.Queries
{
    public class GetTradeStreamByIdContext : QueryContext
    {
        public int Id { get; set; }
    }
}