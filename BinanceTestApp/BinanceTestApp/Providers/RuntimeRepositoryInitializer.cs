using BinanceTestApp.Infrastructure.Interfaces;

namespace BinanceTestApp.Providers
{
    public abstract class RuntimeRepositoryInitializer
    {
        protected readonly IRuntimeRepository RuntimeRepository;

        protected RuntimeRepositoryInitializer(IRuntimeRepository runtimeRepository)
        {
            RuntimeRepository = runtimeRepository;
        }
    }
}