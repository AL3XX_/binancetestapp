using System.Threading.Tasks;
using BinanceTestApp.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BinanceTestApp.Infrastructure.Repositories
{
    public class RuntimeRepository : IRuntimeRepository
    {
        private DbContext _dbContext;

        public RuntimeRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public DbSet<TEntity> Query<TEntity>() where TEntity : class
        {
            return _dbContext.Set<TEntity>();
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}