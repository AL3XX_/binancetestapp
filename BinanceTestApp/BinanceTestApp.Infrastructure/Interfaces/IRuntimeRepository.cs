using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BinanceTestApp.Infrastructure.Interfaces
{
    public interface IRuntimeRepository
    {   
        DbSet<TEntity> Query<TEntity>() where TEntity : class;

        Task SaveAsync();
    }
}