using System;

namespace BinanceTestApp.Database.Entities
{
    public class TradeStream
    {
        public int Id { get; set; }
        
        public long TradeId { get; set; }
        
        public DateTime EventTime { get; set; }
        
        public string EventType { get; set; }
        
        public long FirstTradeId { get; set; }
        
        public long LastTradeId { get; set; }
        
        public Decimal Price { get; set; }
        
        public Decimal Quantity { get; set; }
        
        public string Symbol { get; set; }
        
        public DateTime TradeTime { get; set; }
                
        public bool IsBuyerMaker { get; set; }
        
        public bool Ignore { get; set; }
    }
}