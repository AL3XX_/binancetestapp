using BinanceTestApp.Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BinanceTestApp.Database.Contexts
{
    public class BinanceDatabaseContext : DbContext
    {
        public DbSet<TradeStream> TradeStreams { get; set; }
        
        public BinanceDatabaseContext(DbContextOptions optionsBuilder) : base(optionsBuilder)
        {
        }
    }
}