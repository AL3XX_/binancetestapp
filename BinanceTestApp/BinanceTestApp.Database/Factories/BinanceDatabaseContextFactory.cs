using System;
using System.Collections.Generic;
using System.IO;
using BinanceTestApp.Database.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BinanceTestApp.Database.Factories
{
    public class BinanceDatabaseContextFactory : IDesignTimeDbContextFactory<BinanceDatabaseContext>
    {
        public BinanceDatabaseContext CreateDbContext(string[] args)
        {
            return new BinanceDatabaseContext(DbContextOptionsFactory.Get<BinanceDatabaseContext>());
        }
    }
}