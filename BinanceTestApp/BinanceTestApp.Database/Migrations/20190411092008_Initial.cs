﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BinanceTestApp.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TradeStreams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TradeId = table.Column<long>(nullable: false),
                    EventTime = table.Column<DateTime>(nullable: false),
                    EventType = table.Column<string>(nullable: true),
                    FirstTradeId = table.Column<long>(nullable: false),
                    LastTradeId = table.Column<long>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    Symbol = table.Column<string>(nullable: true),
                    TradeTime = table.Column<DateTime>(nullable: false),
                    IsBuyerMaker = table.Column<bool>(nullable: false),
                    Ignore = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeStreams", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TradeStreams");
        }
    }
}
