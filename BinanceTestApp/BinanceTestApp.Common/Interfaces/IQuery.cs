using System.Threading.Tasks;

namespace BinanceTestApp.Common.Interfaces
{
    public interface IQuery<in TContext, TReturnable>
    {
        Task<TReturnable> QueryAsync(TContext context);
    }
}