using System.Threading.Tasks;
using BinanceTestApp.Common.Contexts;
using BinanceTestApp.Common.Models;

namespace BinanceTestApp.Common.Interfaces
{
    public interface ICommandBuilder
    {
        Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : CommandContext;
    }
}