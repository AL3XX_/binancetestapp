using System.Threading.Tasks;
using BinanceTestApp.Common.Contexts;

namespace BinanceTestApp.Common.Interfaces
{
    public interface IQueryBuilder
    {
        Task<TReturnable> QueryAsync<TContext, TReturnable>(TContext context) where TContext : QueryContext;
    }
}