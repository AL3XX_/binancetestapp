using System.Threading.Tasks;
using BinanceTestApp.Common.Contexts;
using BinanceTestApp.Common.Models;

namespace BinanceTestApp.Common.Interfaces
{
    public interface ICommand<in TContext> where TContext : CommandContext
    {
        Task<CommandResult> ExecuteAsync(TContext context);
    }
}