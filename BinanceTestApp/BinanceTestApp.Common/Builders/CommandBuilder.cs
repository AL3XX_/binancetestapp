using System.Threading.Tasks;
using Autofac;
using BinanceTestApp.Common.Contexts;
using BinanceTestApp.Common.Interfaces;
using BinanceTestApp.Common.Models;

namespace BinanceTestApp.Common.Builders
{
    public class CommandBuilder : ICommandBuilder
    {
        private static IComponentContext _componentContext;

        public CommandBuilder(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }
        
        public async Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : CommandContext
        {
            return await _componentContext.Resolve<ICommand<TContext>>().ExecuteAsync(context);
        }
    }
}