using BinanceTestApp.Common.Enums;

namespace BinanceTestApp.Common.Models
{
    public class CommandResult
    {        
        public CommandResultStatus Status { get; set; }
    }
}